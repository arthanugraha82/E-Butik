package ce.studio.e_butik;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Pesanan extends AppCompatActivity {

    private static final String TAG_RESULT = "result";
    private static final String TAG_IDPESANAN = "idpesanan";
    private static final String TAG_KODEPESANAN = "kodepesanan";
    private static final String TAG_NAMAPRODUK = "namaproduk";
    private static final String TAG_STATUSPESANAN = "statuspesanan";
    private static final String TAG_VALIDPEMBAYARAN = "validpembayaran";
    private static final String TAG_HARGAPESAN = "hargapesan";
    private static final String TAG_JUMLAHPESAN = "jumlahpesan";
    private static final String TAG_TOTALONGKIR = "totalongkir";
    private static final String TAG_GAMBAR = "gambar";

    public static String FEED_URL;

    ProgressDialog pd;

    private GridView mGridView;
    private GridViewAdapterDaftarPesanan mGridAdapterProduk;
    private ArrayList<GridItemPesanan> mGridDataProduk;

    private static final String TAG = DaftarProduk.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pesanan);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mGridView = (GridView) findViewById(R.id.gridView1);

        FEED_URL = Config.LISTPESANAN_URL;
        FEED_URL += "?iduser="+getSharedPreferences("DATA",MODE_PRIVATE).getString("iduser","");

        Log.e("List Pesanan =",FEED_URL);

        //Initialize with empty data
        mGridDataProduk = new ArrayList<>();
        mGridAdapterProduk = new GridViewAdapterDaftarPesanan(Pesanan.this, R.layout.grid_item_daftar_pesanan, mGridDataProduk);
        mGridView.setAdapter(mGridAdapterProduk);

        pd = new ProgressDialog(Pesanan.this);
        pd.setMessage("Sedang mengambil data, tunggu sebentar...");
        pd.setCancelable(false);
        pd.show();

        //Start download
        new getPesanan().execute(FEED_URL);

        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {

                //Get item at position
                GridItemPesanan item = (GridItemPesanan) parent.getItemAtPosition(position);

                SharedPreferences.Editor editor = Pesanan.this.getSharedPreferences("DATA", Context.MODE_PRIVATE).edit();
                editor.putString("idpesanan", item.getIdpesanan());
                editor.putString("kodepesanan", item.getKodepesanan());
                editor.commit();

                Intent intent = new Intent(Pesanan.this, DetailPesanan.class);
                startActivity(intent);
                finish();
            }
        });
    }

    //Downloading data asynchronously
    public class getPesanan extends AsyncTask<String, Void, Integer> {

        @Override
        protected Integer doInBackground(String... params) {
            Integer result1 = 0;
            try {
                // Create Apache HttpClient
                HttpClient httpclient1 = new DefaultHttpClient();
                HttpResponse httpResponse1 = httpclient1.execute(new HttpGet(params[0]));
                int statusCode1 = httpResponse1.getStatusLine().getStatusCode();

                // 200 represents HTTP OK
                if (statusCode1 == 200) {
                    String response1 = streamToString1(httpResponse1.getEntity().getContent());
                    parseResult1(response1);
                    result1 = 1; // Successful
                } else {
                    result1 = 0; //"Failed
                }
            } catch (Exception e) {
                Log.d(TAG, e.getLocalizedMessage());
            }
            return result1;
        }

        @Override
        protected void onPostExecute(Integer result1) {
            // Download complete. Let us update UI
            if (result1 == 1) {
                mGridAdapterProduk.setGridData(mGridDataProduk);

                pd.hide();
            } else {
//                Toast.makeText(Pesanan.this, "Failed to fetch data!", Toast.LENGTH_SHORT).show();
                pd.hide();
            }


        }
    }

    public boolean onOptionsItemSelected(MenuItem item){
        if(item.getItemId()==android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }

    String streamToString1(InputStream stream1) throws IOException {
        BufferedReader bufferedReader1 = new BufferedReader(new InputStreamReader(stream1));
        String line1;
        String result1 = "";
        while ((line1 = bufferedReader1.readLine()) != null) {
            result1 += line1;
        }

        // Close stream
        if (null != stream1) {
            stream1.close();
        }
        return result1;
    }

    /**
     * Parsing the feed results and get the list
     *
     * @param result1
     */
    private void parseResult1(String result1) {
        try {
            JSONObject response1 = new JSONObject(result1);
            JSONArray posts1 = response1.optJSONArray(TAG_RESULT);
            GridItemPesanan item1;

            for (int i = 0; i < posts1.length(); i++) {
                JSONObject post1 = posts1.optJSONObject(i);
                String stridpesanan = post1.getString(TAG_IDPESANAN);
                String strkodepesanan = post1.getString(TAG_KODEPESANAN);
                String strnamaproduk = post1.getString(TAG_NAMAPRODUK);
                String strstatuspesanan = post1.getString(TAG_STATUSPESANAN);
                String strvalidpembayaran = post1.getString(TAG_VALIDPEMBAYARAN);
                String strhargapesan = post1.getString(TAG_HARGAPESAN);
                String strjumlahpesan = post1.getString(TAG_JUMLAHPESAN);
                String strtotalongkir = post1.getString(TAG_TOTALONGKIR);
                item1 = new GridItemPesanan();
                item1.setIdpesanan(stridpesanan);
                item1.setKodepesanan(strkodepesanan);
                item1.setNamaproduk(strnamaproduk);
                item1.setHargapesan(strhargapesan);
                item1.setJumlahpesan(strjumlahpesan);
                item1.setTotalongkir(strtotalongkir);
                if(strstatuspesanan.equals("0")){
                    item1.setStatuspesanan("Rincian pesanan diterima, selanjutnya lakukan pembayaran sebelum "+strvalidpembayaran+". Terimakasih");
                }
                else if(strstatuspesanan.equals("1")){
                    item1.setStatuspesanan("Pembayaran telah dikonfirmasi dan pesanan diterima oleh penjual. Terimakasih");
                }
                else if(strstatuspesanan.equals("2")){
                    item1.setStatuspesanan("Penjual telah menerima rincian pesanan. Terimakasih");
                }
                else if(strstatuspesanan.equals("3")){
                    item1.setStatuspesanan("Penjual telah mengirimkan pesanan. Terimakasih");
                }
                item1.setValidpembayaran(strvalidpembayaran);
                if (null != posts1 && posts1.length() > 0) {
                    JSONObject attachment = posts1.getJSONObject(i);
                    if (attachment != null)
                        item1.setGambar(attachment.getString(TAG_GAMBAR));
                }

                mGridDataProduk.add(item1);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}

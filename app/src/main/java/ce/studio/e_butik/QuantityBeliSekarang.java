package ce.studio.e_butik;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;

import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Locale;

public class QuantityBeliSekarang extends Dialog {
    public Activity c;
    ImageView decjumlahpesan,incjumlahpesan,gambar;
    EditText jumlahpesan,keterangantambahan;
    Button btnsimpan;
    TextView namaproduk,hargaproduk,bahanproduk;

    private static final String TAG_RESULT = "result";
    private static final String TAG_KERANJANG = "keranjang";
    static boolean a=false;

    JSONArray rs = null;

    public static String FEED_URL;

    ProgressDialog pd;

    public QuantityBeliSekarang(Activity a) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = a;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.layoutquantity);
        namaproduk = (TextView) findViewById(R.id.namaproduk);
        hargaproduk = (TextView) findViewById(R.id.hargaproduk);
        bahanproduk = (TextView) findViewById(R.id.bahanproduk);
        jumlahpesan = (EditText) findViewById(R.id.jumlahpesan);
        keterangantambahan = (EditText) findViewById(R.id.keterangantambahan);
        decjumlahpesan = (ImageView) findViewById(R.id.decjumlahpesan);
        incjumlahpesan = (ImageView) findViewById(R.id.incjumlahpesan);
        gambar = (ImageView) findViewById(R.id.gambar);
        btnsimpan = (Button) findViewById(R.id.btnsimpan);

        namaproduk.setText(c.getSharedPreferences("DATA",Context.MODE_PRIVATE).getString("namaproduk",""));
        hargaproduk.setText("Rp. "+String.valueOf(NumberFormat.getNumberInstance(Locale.US).format(Integer.parseInt((c.getSharedPreferences("DATA",Context.MODE_PRIVATE).getString("hargaproduk",""))))).replace(",","."));
        bahanproduk.setText("Bahan : "+c.getSharedPreferences("DATA",Context.MODE_PRIVATE).getString("bahanproduk",""));

        Picasso.with(getContext()).load(c.getSharedPreferences("DATA",Context.MODE_PRIVATE).getString("gambarproduk","")).resize(100,100).into(gambar);

        decjumlahpesan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if((Integer.parseInt(jumlahpesan.getText().toString())-1)<0){
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
                    builder1.setMessage("Apa anda ingin menghapus produk ini?");
                    builder1.setCancelable(false);

                    builder1.setPositiveButton(
                            "Ya",
                            new OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    hapusKeranjang();
                                    dialog.cancel();
                                }
                            });

                    builder1.setNegativeButton(
                            "Tidak",
                            new OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });

                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                }
                else{
                    jumlahpesan.setText(String.valueOf(Integer.parseInt(jumlahpesan.getText().toString())-1));
                }

            }
        });

        incjumlahpesan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                jumlahpesan.setText(String.valueOf(Integer.parseInt(jumlahpesan.getText().toString())+1));
            }
        });

        btnsimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if((Integer.parseInt(jumlahpesan.getText().toString()))==0){
                    Toast.makeText(getContext(), "Pemesanan minimal 1 pcs", Toast.LENGTH_SHORT).show();
                }
                else if((Integer.parseInt(jumlahpesan.getText().toString()))<Integer.parseInt(getContext().getSharedPreferences("DATA",Context.MODE_PRIVATE).getString("stokproduk",""))){
                    Toast.makeText(getContext(), "Pemesanan melebihi stok", Toast.LENGTH_SHORT).show();
                }
                else if(keterangantambahan.getText().toString().equals("")){
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
                    builder1.setMessage("Anda yakin tidak menambahkan keterangan apapun?");
                    builder1.setCancelable(false);

                    builder1.setPositiveButton(
                            "Ya",
                            new OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    kirimKeranjang();
                                    dialog.cancel();
                                    DetailProduk.fa.finish();
                                }
                            });

                    builder1.setNegativeButton(
                            "Tidak",
                            new OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });

                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                }
                else{
                    kirimKeranjang();
                }

            }
        });
    }

    public void kirimKeranjang(){

        final String stridproduk = c.getSharedPreferences("DATA",Context.MODE_PRIVATE).getString("idproduk","").trim();
        final String striduser = c.getSharedPreferences("DATA",Context.MODE_PRIVATE).getString("iduser","").trim();
        final String strjumlahpesan = jumlahpesan.getText().toString().trim();
        final String strhargaproduk = c.getSharedPreferences("DATA",Context.MODE_PRIVATE).getString("hargaproduk","").trim();
        final String strketerangan = keterangantambahan.getText().toString().trim();

        class kirimKeranjang extends AsyncTask<Void,Void,String>{
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                pd = ProgressDialog.show(getContext(),"Tunggu sebentar.....","Mengirim",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                Log.e("Hasil Pos : ",s);
                pd.dismiss();
                if(s.equals("0")){
                    Toast.makeText(getContext(), "Gagal menambah pesanan", Toast.LENGTH_SHORT).show();
                }
                else{
                    c.startActivity(new Intent(getContext(),Keranjang.class));
                    Toast.makeText(getContext(), "Sukses menambah pesanan", Toast.LENGTH_SHORT).show();
                    dismiss();
                }
            }

            @Override
            protected String doInBackground(Void... params) {
                RequestHandler rh = new RequestHandler();
                HashMap<String,String> param = new HashMap<String,String>();
                param.put("idproduk",stridproduk);
                param.put("iduser",striduser);
                param.put("jumlahpesan",strjumlahpesan);
                param.put("hargaproduk",strhargaproduk);
                param.put("keterangan",strketerangan);

                String result = rh.sendPostRequest(Config.KERANJANG_URL, param);

                return result;
            }
        }
        kirimKeranjang u = new kirimKeranjang();
        u.execute();
    }

    public void hapusKeranjang(){

        final String stridproduk = c.getSharedPreferences("DATA",Context.MODE_PRIVATE).getString("idproduk","").trim();
        final String striduser = c.getSharedPreferences("DATA",Context.MODE_PRIVATE).getString("iduser","").trim();

        class hapusKeranjang extends AsyncTask<Void,Void,String>{
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                pd = ProgressDialog.show(getContext(),"Tunggu sebentar.....","Menghapus",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                Log.e("Hasil Pos : ",s);
                pd.dismiss();
                if(s.equals("0")){
                    Toast.makeText(getContext(), "Gagal menghapus pesanan", Toast.LENGTH_SHORT).show();
                }
                else{
                    Toast.makeText(getContext(), "Sukses menghapus pesanan", Toast.LENGTH_SHORT).show();
                    dismiss();
                }
            }

            @Override
            protected String doInBackground(Void... params) {
                RequestHandler rh = new RequestHandler();
                HashMap<String,String> param = new HashMap<String,String>();
                param.put("idproduk",stridproduk);
                param.put("iduser",striduser);

                String result = rh.sendPostRequest(Config.HAPUSKERANJANG_URL, param);

                return result;
            }
        }
        hapusKeranjang u = new hapusKeranjang();
        u.execute();
    }
}

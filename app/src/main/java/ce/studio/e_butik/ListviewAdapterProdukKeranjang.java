package ce.studio.e_butik;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.NumberFormat;
import java.util.Locale;

public class ListviewAdapterProdukKeranjang extends ArrayAdapter<String> {
    private String[] namaproduk;
    private String[] hargaproduk;
    private String[] jumlahpesanproduk;
    private String[] gambar;
    private Activity context;

    public ListviewAdapterProdukKeranjang(Activity context, String[] namaproduk, String[] hargaproduk, String[] jumlahpesanproduk, String[] gambar) {
        super(context, R.layout.customlistviewprodukkeranjang, namaproduk);
        this.context = context;
        this.namaproduk = namaproduk;
        this.hargaproduk = hargaproduk;
        this.jumlahpesanproduk = jumlahpesanproduk;
        this.gambar = gambar;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View listViewItem = inflater.inflate(R.layout.customlistviewprodukkeranjang, null, true);
        TextView Namaproduk = (TextView) listViewItem.findViewById(R.id.tvNamaProduk);
        TextView Hargaproduk = (TextView) listViewItem.findViewById(R.id.tvHargaProduk);
        TextView Jumlahpesanproduk = (TextView) listViewItem.findViewById(R.id.tvJumlahPesan);
        ImageView image = (ImageView) listViewItem.findViewById(R.id.tvGambarProduk);

        Namaproduk.setText(namaproduk[position]);
        Hargaproduk.setText("Rp. "+String.valueOf(NumberFormat.getNumberInstance(Locale.US).format(Integer.parseInt((String.valueOf(hargaproduk[position]))))).replace(",","."));
        Jumlahpesanproduk.setText(jumlahpesanproduk[position]+"x");
        Picasso.with(getContext()).load(gambar[position]).resize(100,100).into(image);

        return  listViewItem;
    }
}
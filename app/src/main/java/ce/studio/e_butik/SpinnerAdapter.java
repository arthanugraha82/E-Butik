package ce.studio.e_butik;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class SpinnerAdapter extends BaseAdapter {
    Context context;
    String iditem[];
    String[] namaitem;
    LayoutInflater inflter;

    public SpinnerAdapter(Context applicationContext, String[] iditem, String[] namaitem) {
        this.context = applicationContext;
        this.iditem = iditem;
        this.namaitem = namaitem;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return iditem.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.customspinner, null);
        TextView id = (TextView) view.findViewById(R.id.iditem);
        TextView names = (TextView) view.findViewById(R.id.namaitem);
        id.setText(iditem[i]);
        names.setText(namaitem[i]);
        return view;
    }
}
package ce.studio.e_butik;

/**
 * Created by Artha on 2/2/2017.
 */
public class GridItemPengiriman {

    String namaservice,hargaservice,estimasiservice,gambar;

    public String getNamaservice() {
        return namaservice;
    }

    public void setNamaservice(String namaservice) {
        this.namaservice = namaservice;
    }

    public String getHargaservice() {
        return hargaservice;
    }

    public void setHargaservice(String hargaservice) {
        this.hargaservice = hargaservice;
    }

    public String getEstimasiservice() {
        return estimasiservice;
    }

    public void setEstimasiservice(String estimasiservice) {
        this.estimasiservice = estimasiservice;
    }

    public String getGambar() {
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }
}
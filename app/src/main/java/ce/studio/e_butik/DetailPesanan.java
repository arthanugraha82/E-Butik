package ce.studio.e_butik;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Locale;

public class DetailPesanan extends AppCompatActivity {

    TextView kodepesanan,namalengkap,nomortelepon,alamatpengiriman,kabupatenpengiriman,provinsipengiriman,namarekeningpengirim,nomorrekeningpengirim,namabankpengirim,jumlahproduk,hargatotal,namaexpedisi,namapengiriman,hargapengiriman;
    ExpandableHeightListView listproduk;


    private static final String TAG_RESULT = "result";
    private static final String TAG_KODEPESANAN= "kodepesanan";
    private static final String TAG_NAMALENGKAP= "namalengkap";
    private static final String TAG_NOMORTELP= "nomortelp";
    private static final String TAG_ALAMATPENGIRIMAN= "alamatpengiriman";
    private static final String TAG_KABUPATENPENGIRIMAN = "kabupatenpengiriman";
    private static final String TAG_PROVINSIPENGIRIMAN= "provinsipengiriman";
    private static final String TAG_NAMAEXPEDISI= "namaexpedisi";
    private static final String TAG_NAMAPENGIRIMAN= "namapengiriman";
    private static final String TAG_HARGAPENGIRIMAN= "hargapengiriman";
    private static final String TAG_NAMAREKENINGPENGIRIM= "namarekeningpengirim";
    private static final String TAG_NOMORREKENINGPENGIRIM= "nomorrekeningpengirim";
    private static final String TAG_NAMABANKPENGIRIM= "namabankpengirim";
    private static final String TAG_NAMAPRODUK= "namaproduk";
    private static final String TAG_HARGAPRODUK= "hargaproduk";
    private static final String TAG_JUMLAHPESANAN= "jumlahpesanan";
    private static final String TAG_GAMBARPRODUK= "gambarproduk";
    private static final String TAG_STATUSPESANAN= "statuspesanan";
    private static final String TAG_STATUSKONFIRMASI= "statuskonfirmasi";
    static boolean a=false;

    JSONArray rs = null;

    public static String FEED_URL;

    ProgressDialog pd;

    String strhargapengiriman;

    Button btnUploadPembayaran,btnBatal;

    TextView namabank, norek;

    ImageView gambar;

    private Uri fileUri1;

    private int PICK_IMAGE_REQUEST = 1;

    private Bitmap bitmap1;

    String strimage1;

    private String filePath = null;

    private static final String TAG = DetailPesanan.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_pesanan);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        kodepesanan = (TextView) findViewById(R.id.kodepesanan);
        namalengkap = (TextView) findViewById(R.id.namalengkap);
        nomortelepon = (TextView) findViewById(R.id.nomortelepon);
        alamatpengiriman = (TextView) findViewById(R.id.alamatpengiriman);
        kabupatenpengiriman = (TextView) findViewById(R.id.kabupatenpengiriman);
        provinsipengiriman = (TextView) findViewById(R.id.provinsipengiriman);
        namarekeningpengirim = (TextView) findViewById(R.id.namarekeningpengirim);
        nomorrekeningpengirim = (TextView) findViewById(R.id.nomorrekeningpengirim);
        namabankpengirim = (TextView) findViewById(R.id.namabankpengirim);
        jumlahproduk = (TextView) findViewById(R.id.jumlahproduk);
        hargatotal = (TextView) findViewById(R.id.hargatotal);
        namaexpedisi = (TextView) findViewById(R.id.namaexpedisi);
        namapengiriman = (TextView) findViewById(R.id.namapengiriman);
        hargapengiriman = (TextView) findViewById(R.id.hargapengiriman);
        listproduk = (ExpandableHeightListView) findViewById(R.id.listproduk);
        btnUploadPembayaran = (Button) findViewById(R.id.btnUploadPembayaran);
        btnBatal = (Button) findViewById(R.id.btnBatal);

        listproduk.setDivider(null);

        listproduk.setExpanded(true);

        new getData().execute();

        btnUploadPembayaran.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isStoragePermissionGrantedGaleri1();
            }
        });

        btnBatal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new delete().execute();
            }
        });

    }

    private class delete extends AsyncTask<String, String, JSONObject> {
        @Override
        protected void onPreExecute(){
            pd = new ProgressDialog(DetailPesanan.this);
            pd.setMessage("Sedang menghapus data, tunggu sebentar...");
            pd.setCancelable(false);
            pd.show();
        }
        @Override
        protected JSONObject doInBackground(String... args)
        {
            FEED_URL = Config.DELETE_URL;
            FEED_URL += "?kodepesanan="+getSharedPreferences("DATA",MODE_PRIVATE).getString("kodepesanan","");

            Log.e("Hapus Pesanan =",FEED_URL);

            JSONParser jParser = new JSONParser();

            JSONObject json = jParser.getJSONFromUrl(FEED_URL);
            if(json==null)
            {
                a=false;
            }
            else a=true;
            return json;
        }
        protected void onPostExecute(JSONObject json) {
            if(a==true)
            {
                try{
                    Log.e("status",a+"");
                    rs = json.getJSONArray(TAG_RESULT);

                    for(int i=0; i<rs.length();i++)
                    {
                        JSONObject a = rs.getJSONObject(i);
                        String res = a.getString(TAG_RESULT);

                        if (res.equals("1")){
                            Toast.makeText(DetailPesanan.this, "Berhasil menghapus pesanan", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                        else{
                            Toast.makeText(DetailPesanan.this, "Gagal menghapus pesanan", Toast.LENGTH_SHORT).show();
                        }
                        pd.hide();
                    }

                }catch(JSONException e)
                {
                    e.printStackTrace();
                }
            }
            else {

                Toast.makeText(DetailPesanan.this, "Server Maintenance", Toast.LENGTH_SHORT).show();
                Log.e("status",a+"");
                pd.hide();
            }

        }

    }

    private class getData extends AsyncTask<String, String, JSONObject> {
        @Override
        protected void onPreExecute(){

        }
        @Override
        protected JSONObject doInBackground(String... args)
        {
            FEED_URL = Config.DETAILPESANAN_URL;
            FEED_URL += "?kodepesanan="+getSharedPreferences("DATA",MODE_PRIVATE).getString("kodepesanan","");
            FEED_URL += "&iduser="+getSharedPreferences("DATA",MODE_PRIVATE).getString("iduser","");

            Log.e("URL Pesanan =",FEED_URL);

            JSONParser jParser = new JSONParser();

            JSONObject json = jParser.getJSONFromUrl(FEED_URL);
            if(json==null)
            {
                a=false;
            }
            else a=true;
            return json;
        }
        protected void onPostExecute(JSONObject json) {
            if(a==true)
            {
                try{
                    Log.e("status",a+"");
                    rs = json.getJSONArray(TAG_RESULT);
                    String[] namaproduk = new String[rs.length()];
                    String[] hargaproduk = new String[rs.length()];
                    String[] jumlahpesanan = new String[rs.length()];
                    String[] gambarproduk = new String[rs.length()];

                    jumlahproduk.setText(rs.length()+ " produk");
                    int totalbayar=0;

                    for(int i=0; i<rs.length();i++)
                    {
                        JSONObject a = rs.getJSONObject(i);
                        String strkodepesanan = a.getString(TAG_KODEPESANAN);
                        String strnamalengkap = a.getString(TAG_NAMALENGKAP);
                        String strnomortelp = a.getString(TAG_NOMORTELP);
                        String stralamatpengiriman = a.getString(TAG_ALAMATPENGIRIMAN);
                        String strkabupatenpengiriman = a.getString(TAG_KABUPATENPENGIRIMAN);
                        String strprovinsipengiriman = a.getString(TAG_PROVINSIPENGIRIMAN);
                        String strnamaexpedisi = a.getString(TAG_NAMAEXPEDISI);
                        String strnamapengiriman = a.getString(TAG_NAMAPENGIRIMAN);
                        strhargapengiriman = a.getString(TAG_HARGAPENGIRIMAN);
                        String strnamarekeningpengirim = a.getString(TAG_NAMAREKENINGPENGIRIM);
                        String strnomorrekeningpengirim = a.getString(TAG_NOMORREKENINGPENGIRIM);
                        String strnamabankpengirim = a.getString(TAG_NAMABANKPENGIRIM);
                        String strnamaproduk = a.getString(TAG_NAMAPRODUK);
                        String strhargaproduk = a.getString(TAG_HARGAPRODUK);
                        String strjumlahpesanan = a.getString(TAG_JUMLAHPESANAN);
                        String strgambarproduk = a.getString(TAG_GAMBARPRODUK);
                        String strstatuspesanan = a.getString(TAG_STATUSPESANAN);
                        String strstatuskonfirmasi = a.getString(TAG_STATUSKONFIRMASI);

                        totalbayar = totalbayar+(Integer.parseInt(strjumlahpesanan)*Integer.parseInt(strhargaproduk));

                        namaproduk[i] = strnamaproduk;
                        hargaproduk[i] = strhargaproduk;
                        jumlahpesanan[i] = strjumlahpesanan;
                        gambarproduk[i] = strgambarproduk;

                        kodepesanan.setText("Kode #"+strkodepesanan);
                        namalengkap.setText(strnamalengkap);
                        nomortelepon.setText(strnomortelp);
                        alamatpengiriman.setText(stralamatpengiriman);
                        kabupatenpengiriman.setText(strkabupatenpengiriman);
                        provinsipengiriman.setText(strprovinsipengiriman);
                        namarekeningpengirim.setText(strnamarekeningpengirim);
                        nomorrekeningpengirim.setText(strnomorrekeningpengirim);
                        namabankpengirim.setText(strnamabankpengirim);
                        namaexpedisi.setText(strnamaexpedisi);
                        namapengiriman.setText(strnamapengiriman);
                        hargapengiriman.setText("Rp. "+String.valueOf(NumberFormat.getNumberInstance(Locale.US).format(Integer.parseInt((strhargapengiriman)))).replace(",","."));

                        if(strstatuskonfirmasi.equals("0")){
                            btnUploadPembayaran.setVisibility(View.VISIBLE);
                            btnBatal.setVisibility(View.VISIBLE);
                        }
                        else{
                            btnUploadPembayaran.setVisibility(View.GONE);
                            btnBatal.setVisibility(View.GONE);
                        }
                    }
                    totalbayar = totalbayar+Integer.parseInt(strhargapengiriman);
                    hargatotal.setText("Rp. "+String.valueOf(NumberFormat.getNumberInstance(Locale.US).format(totalbayar)).replace(",","."));

                    ListviewAdapterProdukKeranjang customList = new ListviewAdapterProdukKeranjang(DetailPesanan.this, namaproduk, hargaproduk, jumlahpesanan, gambarproduk);

                    listproduk.setAdapter(customList);

                }catch(JSONException e)
                {
                    e.printStackTrace();
                }
            }
            else {

                Toast.makeText(getApplicationContext(), "Server Maintenance", Toast.LENGTH_SHORT).show();
                Log.e("status",a+"");
            }

        }

    }

    public  boolean isStoragePermissionGrantedGaleri1() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG,"Permission is granted");
                showFileChooser1();
                return true;
            } else {

                Log.v(TAG,"Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG,"Permission is granted");
            showFileChooser1();
            return true;
        }
    }

    public void uploadImageGaleri(){

        if (fileUri1!=null){
            strimage1 = getStringImage(bitmap1);
        }

        class uploadImageGaleri extends AsyncTask<Void,Void,String>{
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                pd = ProgressDialog.show(DetailPesanan.this,"Tunggu sebentar.....","Mengunggah",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                pd.dismiss();
                if(s.equals("0")){
                    Toast.makeText(DetailPesanan.this, "Gagal Mengirim Bukti !", Toast.LENGTH_SHORT).show();
                }
                else{
                    Toast.makeText(DetailPesanan.this, "Sukses Mengirim Bukti!", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }

            @Override
            protected String doInBackground(Void... params) {
                RequestHandler rh = new RequestHandler();
                HashMap<String,String> param = new HashMap<String,String>();
                param.put("kodepesanan",getSharedPreferences("DATA",MODE_PRIVATE).getString("kodepesanan",""));

                if (strimage1!=null){
                    param.put("image1",strimage1);
                }

                String result = rh.sendPostRequest(Config.KIRIMBUKTI_URL, param);

                return result;
            }
        }
        uploadImageGaleri u = new uploadImageGaleri();
        u.execute();
    }

    private void showFileChooser1() {
        Intent intent = new Intent();
        filePath = "1";
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Ambil Gambar"), PICK_IMAGE_REQUEST);
    }

    /**
     * Receiving activity result method will be called after closing the camera
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_IMAGE_REQUEST){
            if (resultCode == RESULT_OK && data != null && data.getData() != null) {
                if (filePath != null && filePath.equals("1")) {
                    fileUri1 = data.getData();
                    try {
                        bitmap1 = MediaStore.Images.Media.getBitmap(getContentResolver(), fileUri1);
                        uploadImageGaleri();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
//                    Toast.makeText(getApplicationContext(),
//                            "Sorry, file path is missing!", Toast.LENGTH_LONG).show();
                }

            } else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(getApplicationContext(),
                        "User cancelled image capture", Toast.LENGTH_SHORT)
                        .show();

            } else {
                Toast.makeText(getApplicationContext(),
                        "Sorry! Failed to capture image", Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }

    public String getStringImage(Bitmap bmp){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }
}

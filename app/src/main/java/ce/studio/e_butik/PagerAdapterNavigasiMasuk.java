package ce.studio.e_butik;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class PagerAdapterNavigasiMasuk extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public PagerAdapterNavigasiMasuk(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                Login tab0 = new Login();

                return tab0;
            case 1:
                Register tab1 = new Register();
                return tab1;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
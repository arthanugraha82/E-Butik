package ce.studio.e_butik;

import android.app.Activity;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

public class GridViewAdapterPengiriman extends ArrayAdapter<GridItemPengiriman> {

    private Context mContext;
    private int layoutResourceId;
    private ArrayList<GridItemPengiriman> mGridData = new ArrayList<GridItemPengiriman>();

    public GridViewAdapterPengiriman(Context mContext, int layoutResourceId, ArrayList<GridItemPengiriman> mGridData) {
        super(mContext, layoutResourceId, mGridData);
        this.layoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.mGridData = mGridData;
    }


    /**
     * Updates grid data and refresh grid items.
     * @param mGridData
     */
    public void setGridData(ArrayList<GridItemPengiriman> mGridData) {
        this.mGridData = mGridData;
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder holder;

        if (row == null) {
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            holder = new ViewHolder();
            holder.namaservice = (TextView) row.findViewById(R.id.tvNamaService);
            holder.hargaservice = (TextView) row.findViewById(R.id.tvHargaService);
            holder.estimasiservice = (TextView) row.findViewById(R.id.tvEstimasiService);
            holder.gambar = (ImageView) row.findViewById(R.id.gambarservice);
            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();
        }

        GridItemPengiriman item = mGridData.get(position);
        holder.hargaservice.setText("Rp. "+String.valueOf(NumberFormat.getNumberInstance(Locale.US).format(Integer.parseInt((item.getHargaservice())))).replace(",","."));
        holder.namaservice.setText(Html.fromHtml(item.getNamaservice()));
        if(!item.getEstimasiservice().equals("")){
            holder.estimasiservice.setText(Html.fromHtml(item.getEstimasiservice())+" hari");
        }
        else {
            holder.estimasiservice.setText(Html.fromHtml(item.getEstimasiservice()));
        }
        if(item.getGambar().equals("JNE")){
            holder.gambar.setBackgroundResource(R.drawable.jne);
        }
        else if (item.getGambar().equals("TIKI")){
            holder.gambar.setBackgroundResource(R.drawable.tiki);
        }
        else{
            holder.gambar.setBackgroundResource(R.drawable.pos);
        }

        return row;
    }

    static class ViewHolder {
        TextView namaservice,hargaservice,estimasiservice;
        ImageView gambar;
    }
}
package ce.studio.e_butik;

public class Config {

    public static String LOGIN_URL="http://butikgaruda.000webhostapp.com//butik/android/login.php";
    public static String REGISTRASI_URL="http://butikgaruda.000webhostapp.com//butik/android/registrasi.php";
    public static String PRODUK_URL="http://butikgaruda.000webhostapp.com//butik/android/daftarproduk.php";
    public static String CARIPRODUK_URL="http://butikgaruda.000webhostapp.com//butik/android/cariproduk.php";
    public static String KATEGORI_URL="http://butikgaruda.000webhostapp.com//butik/android/daftarkategori.php";
    public static String DETAILKATEGORI_URL="http://butikgaruda.000webhostapp.com//butik/android/detaildaftarkategori.php";
    public static String PROVINSI_URL="http://butikgaruda.000webhostapp.com//butik/android/provinsi.php";
    public static String KOTA_URL="http://butikgaruda.000webhostapp.com//butik/android/kota.php";
    public static String BIAYA_URL="http://butikgaruda.000webhostapp.com//butik/android/biayakirim.php";
    public static String DETAILPRODUK_URL="http://butikgaruda.000webhostapp.com//butik/android/detailproduk.php";
    public static String KERANJANG_URL="http://butikgaruda.000webhostapp.com//butik/android/keranjang.php";
    public static String HAPUSKERANJANG_URL="http://butikgaruda.000webhostapp.com//butik/android/hapuskeranjang.php";
    public static String BELI_URL="http://butikgaruda.000webhostapp.com//butik/android/beli.php";
    public static String DAFTARKERANJANG_URL="http://butikgaruda.000webhostapp.com//butik/android/daftarkeranjang.php";
    public static String DAFTARPRODUKKERANJANG_URL="http://butikgaruda.000webhostapp.com//butik/android/daftarprodukkeranjang.php";
    public static String TAMBAHPESANAN_URL="http://butikgaruda.000webhostapp.com//butik/android/tambahpesanan.php";
    public static String LISTPESANAN_URL="http://butikgaruda.000webhostapp.com//butik/android/listpesanan.php";
    public static String DETAILPESANAN_URL="http://butikgaruda.000webhostapp.com//butik/android/detailpesanan.php";
    public static String KIRIMBUKTI_URL="http://butikgaruda.000webhostapp.com//butik/android/kirimbukti.php";
    public static String DELETE_URL="http://butikgaruda.000webhostapp.com//butik/android/deletepesanan.php";

}

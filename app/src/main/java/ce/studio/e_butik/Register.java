package ce.studio.e_butik;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.Image;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Register extends Fragment {

    Button btnregister,btnlogin;

    private static final String TAG_RESULT = "result";
    private static final String TAG_LOGIN = "login";
    private static final String TAG_IDUSER = "iduser";
    private static final String TAG_NAMA = "nama";
    private static final String TAG_EMAIL = "email";

    static boolean a=false;

    JSONArray rs = null;

    public static String FEED_URL;

    EditText email,password,nama,alamat,wilayah,notelp;
    ImageView visiblepassword;

    ProgressDialog pd;

    int visible=0;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.fragment_register, container, false);

        email = (EditText) rootview.findViewById(R.id.email);
        password = (EditText) rootview.findViewById(R.id.password);
        nama = (EditText) rootview.findViewById(R.id.nama);
        alamat = (EditText) rootview.findViewById(R.id.alamat);
        notelp = (EditText) rootview.findViewById(R.id.notelp);
        visiblepassword = (ImageView) rootview.findViewById(R.id.visiblepassword);
        btnregister = (Button) rootview.findViewById(R.id.btnregister);

        btnregister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new register().execute();
            }
        });

        visiblepassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(visible==0){
                    password.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    visible=1;
                    visiblepassword.setBackgroundResource(R.drawable.eye);
                }
                else{
                    password.setInputType(129);
                    visible=0;
                    visiblepassword.setBackgroundResource(R.drawable.noeye);
                }

            }
        });

        return rootview;
    }

    private class register extends AsyncTask<String, String, JSONObject> {
        @Override
        protected void onPreExecute(){
            pd = new ProgressDialog(getActivity());
            pd.setMessage("Sedang mengirim data, tunggu sebentar...");
            pd.setCancelable(false);
            pd.show();
        }
        @Override
        protected JSONObject doInBackground(String... args)
        {
            FEED_URL = Config.REGISTRASI_URL;
            FEED_URL += "?email="+email.getText().toString();
            FEED_URL += "&password="+password.getText().toString();
            FEED_URL += "&nama="+nama.getText().toString().replace(" ","%20");
            FEED_URL += "&alamat="+alamat.getText().toString().replace(" ","%20");
            FEED_URL += "&telepon="+notelp.getText().toString().replace(" ","%20");

            Log.e("Register =",FEED_URL);

            JSONParser jParser = new JSONParser();

            JSONObject json = jParser.getJSONFromUrl(FEED_URL);
            if(json==null)
            {
                a=false;
            }
            else a=true;
            return json;
        }
        protected void onPostExecute(JSONObject json) {
            if(a==true)
            {
                try{
                    Log.e("status",a+"");
                    rs = json.getJSONArray(TAG_RESULT);

                    for(int i=0; i<rs.length();i++)
                    {
                        JSONObject a = rs.getJSONObject(i);
                        String login = a.getString(TAG_LOGIN);

                        if (login.equals("1")){

                            Toast.makeText(getActivity(), "Berhasil melakukan pendaftaran!", Toast.LENGTH_SHORT).show();

                            NavigasiMasuk.fa.finish();


                        }
                        else if (login.equals("2")){
                            Toast.makeText(getActivity(), "Email sudah terdaftar!", Toast.LENGTH_SHORT).show();
                        }
                        else{
                            Toast.makeText(getActivity(), "Gagal melakukan pendaftaran!", Toast.LENGTH_SHORT).show();
                        }
                        pd.hide();
                    }

                }catch(JSONException e)
                {
                    e.printStackTrace();
                }
            }
            else {

                Toast.makeText(getActivity(), "Server Maintenance", Toast.LENGTH_SHORT).show();
                Log.e("status",a+"");
                pd.hide();
            }

        }

    }
}

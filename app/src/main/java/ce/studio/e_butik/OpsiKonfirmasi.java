package ce.studio.e_butik;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;

public class  OpsiKonfirmasi extends AppCompatActivity {

    public static TextView namalengkap,nomortelepon,alamatpengiriman,kabupatenpengiriman,provinsipengiriman,namarekeningpengirim,nomorrekeningpengirim,namabankpengirim,jumlahproduk,hargatotal,namaexpedisi,namapengiriman,hargapengiriman;
    ExpandableHeightListView listproduk;
    Button btnbuatpesanan;

    private static final String TAG_RESULT = "result";
    private static final String TAG_GAMBARPRODUK= "gambar";
    private static final String TAG_NAMAPRODUK= "namaproduk";
    private static final String TAG_HARGAPRODUK = "hargaproduk";
    private static final String TAG_JUMLAHPESAN= "jumlahpesan";
    static boolean a=false;

    JSONArray rs = null;

    public static String FEED_URL;

    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_opsi_konfirmasi);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        namalengkap = (TextView) findViewById(R.id.namalengkap);
        nomortelepon = (TextView) findViewById(R.id.nomortelepon);
        alamatpengiriman = (TextView) findViewById(R.id.alamatpengiriman);
        kabupatenpengiriman = (TextView) findViewById(R.id.kabupatenpengiriman);
        provinsipengiriman = (TextView) findViewById(R.id.provinsipengiriman);
        namarekeningpengirim = (TextView) findViewById(R.id.namarekeningpengirim);
        nomorrekeningpengirim = (TextView) findViewById(R.id.nomorrekeningpengirim);
        namabankpengirim = (TextView) findViewById(R.id.namabankpengirim);
        jumlahproduk = (TextView) findViewById(R.id.jumlahproduk);
        hargatotal = (TextView) findViewById(R.id.hargatotal);
        namaexpedisi = (TextView) findViewById(R.id.namaexpedisi);
        namapengiriman = (TextView) findViewById(R.id.namapengiriman);
        hargapengiriman = (TextView) findViewById(R.id.hargapengiriman);
        listproduk = (ExpandableHeightListView) findViewById(R.id.listproduk);
        btnbuatpesanan = (Button) findViewById(R.id.btnbuatpesanan);

        listproduk.setDivider(null);

        listproduk.setExpanded(true);

        namalengkap.setText(getSharedPreferences("DATA",MODE_PRIVATE).getString("nama",""));
        nomortelepon.setText("( "+getSharedPreferences("DATA",MODE_PRIVATE).getString("telepon","")+" )");
        alamatpengiriman.setText(getSharedPreferences("DATA",MODE_PRIVATE).getString("alamatpengiriman",""));
        kabupatenpengiriman.setText(getSharedPreferences("DATA",MODE_PRIVATE).getString("namakota",""));
        provinsipengiriman.setText(getSharedPreferences("DATA",MODE_PRIVATE).getString("namaprovinsi",""));
        namarekeningpengirim.setText(getSharedPreferences("DATA",MODE_PRIVATE).getString("namarekeningpengirim",""));
        nomorrekeningpengirim.setText(getSharedPreferences("DATA",MODE_PRIVATE).getString("nomorrekeningpengirim",""));
        namabankpengirim.setText(getSharedPreferences("DATA",MODE_PRIVATE).getString("namabankpengirim",""));
        namaexpedisi.setText(getSharedPreferences("DATA",MODE_PRIVATE).getString("namaexpedisi",""));
        namapengiriman.setText(getSharedPreferences("DATA",MODE_PRIVATE).getString("namaservice",""));
        hargapengiriman.setText("Rp. "+String.valueOf(NumberFormat.getNumberInstance(Locale.US).format(Integer.parseInt((getSharedPreferences("DATA",MODE_PRIVATE).getString("hargaservice",""))))).replace(",","."));
        hargatotal.setText("Rp. "+String.valueOf(NumberFormat.getNumberInstance(Locale.US).format(Integer.parseInt((String.valueOf(GridViewAdapterDaftarKeranjang.totalpesanan)))+Integer.parseInt((String.valueOf(getSharedPreferences("DATA",MODE_PRIVATE).getString("hargaservice",""))))).replace(",",".")));

        new getProdukKeranjang().execute();

        btnbuatpesanan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                kirimPesanan();
            }
        });
    }

    public boolean onOptionsItemSelected(MenuItem item){
        if(item.getItemId()==android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }

    private class getProdukKeranjang extends AsyncTask<String, String, JSONObject> {
        @Override
        protected void onPreExecute(){

        }
        @Override
        protected JSONObject doInBackground(String... args)
        {
            FEED_URL = Config.DAFTARPRODUKKERANJANG_URL;
            FEED_URL += "?keranjang="+getSharedPreferences("DATA",MODE_PRIVATE).getString("keranjang","").replace(" ","%20");
            FEED_URL += "&iduser="+getSharedPreferences("DATA",MODE_PRIVATE).getString("iduser","").replace(" ","%20");

            Log.e("URL Keranjang =",FEED_URL);

            JSONParser jParser = new JSONParser();

            JSONObject json = jParser.getJSONFromUrl(FEED_URL);
            if(json==null)
            {
                a=false;
            }
            else a=true;
            return json;
        }
        protected void onPostExecute(JSONObject json) {
            if(a==true)
            {
                try{
                    Log.e("status",a+"");
                    rs = json.getJSONArray(TAG_RESULT);
                    jumlahproduk.setText(String.valueOf(rs.length())+" produk");
                    String[] hargaproduk = new String[rs.length()];
                    String[] namaproduk = new String[rs.length()];
                    String[] gambarproduk = new String[rs.length()];
                    String[] jumlahpesanproduk = new String[rs.length()];

                    for(int i=0; i<rs.length();i++)
                    {
                        JSONObject a = rs.getJSONObject(i);
                        String strhargaproduk = a.getString(TAG_HARGAPRODUK);
                        String strnamaproduk = a.getString(TAG_NAMAPRODUK);
                        String strgambarproduk = a.getString(TAG_GAMBARPRODUK);
                        String strjumlahpesanproduk = a.getString(TAG_JUMLAHPESAN);

                        hargaproduk[i] = strhargaproduk;
                        namaproduk[i] = strnamaproduk;
                        gambarproduk[i] = strgambarproduk;
                        jumlahpesanproduk[i] = strjumlahpesanproduk;

                    }
                    ListviewAdapterProdukKeranjang customList = new ListviewAdapterProdukKeranjang(OpsiKonfirmasi.this, namaproduk, hargaproduk, jumlahpesanproduk, gambarproduk);

                    listproduk.setAdapter(customList);

                }catch(JSONException e)
                {
                    e.printStackTrace();
                }
            }
            else {

                Toast.makeText(getApplicationContext(), "Server Maintenance", Toast.LENGTH_SHORT).show();
                Log.e("status",a+"");
            }

        }

    }

    public void kirimPesanan(){
        Calendar c = Calendar.getInstance();
        String kodepemesanan = String.valueOf(c.get(Calendar.DAY_OF_MONTH));
        kodepemesanan += String.valueOf(c.get(Calendar.MONTH));
        kodepemesanan += String.valueOf(c.get(Calendar.YEAR));
        kodepemesanan += String.valueOf(c.get(Calendar.HOUR_OF_DAY));
        kodepemesanan += String.valueOf(c.get(Calendar.MINUTE));
        kodepemesanan += String.valueOf(c.get(Calendar.SECOND));
        String tanggal = String.valueOf(c.get(Calendar.YEAR)+"-"+c.get(Calendar.MONTH)+"-"+c.get(Calendar.DAY_OF_MONTH)+" "+c.get(Calendar.HOUR_OF_DAY)+":"+c.get(Calendar.MINUTE)+":"+c.get(Calendar.SECOND));
        final String stridproduk = getSharedPreferences("DATA", Context.MODE_PRIVATE).getString("idproduk","").trim();
        final String striduser = getSharedPreferences("DATA",Context.MODE_PRIVATE).getString("iduser","").trim();
        final String strkodepemesanan = kodepemesanan.trim();
        final String strtanggal = tanggal.trim();
        final String strhargapengiriman = getSharedPreferences("DATA",Context.MODE_PRIVATE).getString("hargaservice","").trim();
        final String strnamaprovinsi = getSharedPreferences("DATA",Context.MODE_PRIVATE).getString("namaprovinsi","").trim();
        final String strnamakota = getSharedPreferences("DATA",Context.MODE_PRIVATE).getString("namakota","").trim();
        final String stralamatpengiriman = getSharedPreferences("DATA",Context.MODE_PRIVATE).getString("alamatpengiriman","").trim();
        final String strnamaexpedisi = getSharedPreferences("DATA",Context.MODE_PRIVATE).getString("namaexpedisi","").trim();
        final String strjenisexpedisi = getSharedPreferences("DATA",Context.MODE_PRIVATE).getString("namaservice","").trim();
        final String strtotalpemesanan = String.valueOf(GridViewAdapterDaftarKeranjang.totalpesanan).trim();
        final String strtotalpembayaran = String.valueOf(GridViewAdapterDaftarKeranjang.totalpesanan+Integer.parseInt(getSharedPreferences("DATA",MODE_PRIVATE).getString("hargaservice",""))).trim();
        final String strnamapemilik = getSharedPreferences("DATA",MODE_PRIVATE).getString("namarekeningpengirim","").trim();
        final String strnomorrekening = getSharedPreferences("DATA",MODE_PRIVATE).getString("nomorrekeningpengirim","").trim();
        final String strnamabank = getSharedPreferences("DATA",MODE_PRIVATE).getString("namabankpengirim","").trim();

        class kirimPesanan extends AsyncTask<Void,Void,String>{
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                pd = ProgressDialog.show(OpsiKonfirmasi.this,"Tunggu sebentar.....","Mengirim",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                pd.dismiss();
                if(s.equals("0")){
                    Toast.makeText(OpsiKonfirmasi.this, "Gagal mengirim pesanan", Toast.LENGTH_SHORT).show();
                }
                else{
                    Toast.makeText(OpsiKonfirmasi.this, "Sukses mengirim pesanan", Toast.LENGTH_SHORT).show();
                    Keranjang.close=true;
                    Keranjang.fa.finish();
                    OpsiAlamat.fa.finish();
                    OpsiPengiriman.fa.finish();
                    OpsiPembayaran.fa.finish();
                    startActivity(new Intent(OpsiKonfirmasi.this,Pesanan.class));
                    finish();
                }
            }

            @Override
            protected String doInBackground(Void... params) {
                RequestHandler rh = new RequestHandler();
                HashMap<String,String> param = new HashMap<String,String>();
                param.put("idproduk",stridproduk);
                param.put("iduser",striduser);
                param.put("kodepemesanan",strkodepemesanan);
                param.put("tanggal",strtanggal);
                param.put("hargapengiriman",strhargapengiriman);
                param.put("namaprovinsi",strnamaprovinsi);
                param.put("namakota",strnamakota);
                param.put("alamatpengiriman",stralamatpengiriman);
                param.put("namaexpedisi",strnamaexpedisi);
                param.put("jenisexpedisi",strjenisexpedisi);
                param.put("totalpemesanan",strtotalpemesanan);
                param.put("totalpembayaran",strtotalpembayaran);
                param.put("namapemilik",strnamapemilik);
                param.put("nomorrekening",strnomorrekening);
                param.put("namabank",strnamabank);
                param.put("keranjang",getSharedPreferences("DATA",MODE_PRIVATE).getString("keranjang",""));

                String result = rh.sendPostRequest(Config.TAMBAHPESANAN_URL, param);

                return result;
            }
        }
        kirimPesanan u = new kirimPesanan();
        u.execute();
    }
}

package ce.studio.e_butik;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

public class OpsiPengiriman extends AppCompatActivity {

    Spinner dataexpedisi;

    public static Activity fa;

    private static final String TAG_RESULT = "result";
    private static final String TAG_NAMASERVICE = "namaservice";
    private static final String TAG_HARGASERVICE = "biaya";
    private static final String TAG_ESTIMASISERVICE = "estimasi";

    public static String FEED_URL;

    ProgressDialog pd;

    private GridView mGridView1;
    private GridViewAdapterPengiriman mGridAdapterProduk;
    private ArrayList<GridItemPengiriman> mGridDataProduk;
    GridItemPengiriman item;

    static boolean a=false;

    JSONArray rs = null;
    String myJSON="";
    boolean close=false;

    int prevposisi=0;

    LinearLayout listback;
    TextView namaservice,hargaservice;
    Button btnopsipembayaran;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_opsi_pengiriman);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        fa = this;

        btnopsipembayaran = (Button) findViewById(R.id.btnopsipembayaran);
        dataexpedisi = (Spinner) findViewById(R.id.dataexpedisi);
        mGridView1 = (GridView) findViewById(R.id.gridView1);

        mGridView1.setVisibility(View.INVISIBLE);

        String[] iditem={"0","0","0"};
        String[] namaitem={"JNE","TIKI","POS"};
        SpinnerAdapter customAdapter=new SpinnerAdapter(getApplicationContext(),iditem,namaitem);
        dataexpedisi.setAdapter(customAdapter);

        btnopsipembayaran.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(OpsiPengiriman.this,OpsiPembayaran.class));
            }
        });

        dataexpedisi.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                TextView tvidexpedisi = (TextView) view.findViewById(R.id.iditem);
                TextView tvnamaexpedisi = (TextView) view.findViewById(R.id.namaitem);
                SharedPreferences.Editor editor = getSharedPreferences("DATA", Context.MODE_PRIVATE).edit();
                editor.putString("idexpedisi", tvidexpedisi.getText().toString());
                editor.putString("namaexpedisi", tvnamaexpedisi.getText().toString());
                editor.commit();
                mGridView1.setEnabled(true);
                new getPengiriman().execute();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mGridView1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                listback = (LinearLayout) v.findViewById(R.id.listback);
                item = (GridItemPengiriman) parent.getItemAtPosition(position);
                AlertDialog.Builder builder1 = new AlertDialog.Builder(OpsiPengiriman.this);
                builder1.setMessage("Apa anda ingin menggunakan jasa pengiriman ini?");
                builder1.setCancelable(false);

                builder1.setPositiveButton(
                        "Ya",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                listback.setBackgroundColor(Color.GRAY);
                                mGridView1.setEnabled(false);
                                SharedPreferences.Editor editor = getSharedPreferences("DATA", Context.MODE_PRIVATE).edit();
                                editor.putString("namaservice", item.getNamaservice());
                                editor.putString("hargaservice", item.getHargaservice());
                                editor.commit();
                                dialog.cancel();
                            }
                        });

                builder1.setNegativeButton(
                        "Tidak",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert11 = builder1.create();
                alert11.show();



            }
        });
    }

    public void onBackPressed() {
        finish();
        close=true;
    }

    public boolean onOptionsItemSelected(MenuItem item){
        if(item.getItemId()==android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }

    private class getPengiriman extends AsyncTask<String, String, JSONObject> {
        @Override
        protected void onPreExecute(){
            pd = new ProgressDialog(OpsiPengiriman.this);
            pd.setMessage("Sedang mengambil data, tunggu sebentar...");
            pd.setCancelable(false);
            pd.show();
        }
        @Override
        protected JSONObject doInBackground(String... args)
        {
            FEED_URL = Config.BIAYA_URL;
            FEED_URL += "?destination="+getSharedPreferences("DATA",MODE_PRIVATE).getString("idkota","");
            FEED_URL += "&beratsatuan=0.25";
            FEED_URL += "&jumlahbarang="+GridViewAdapterDaftarKeranjang.jumlahpesanan;
            FEED_URL += "&expedisi="+getSharedPreferences("DATA",MODE_PRIVATE).getString("namaexpedisi","").toLowerCase();

            Log.e("URL Pengiriman =",FEED_URL);

            JSONParser jParser = new JSONParser();

            JSONObject json = jParser.getJSONFromUrl(FEED_URL);
            if(json==null)
            {
                a=false;
            }
            else a=true;
            return json;
        }
        protected void onPostExecute(JSONObject json) {
            if(a==true)
            {
                try{
//                    Log.e("status",a+"");
                    rs = json.getJSONArray(TAG_RESULT);
                    GridItemPengiriman item1;

                    if(String.valueOf(rs).equals(myJSON)){

                    }
                    else{
                        //Initialize with empty data
                        mGridDataProduk = new ArrayList<>();
                        mGridAdapterProduk = new GridViewAdapterPengiriman(OpsiPengiriman.this, R.layout.customlistview, mGridDataProduk);
                        mGridView1.setAdapter(mGridAdapterProduk);

                        mGridDataProduk.clear();

                        for (int i = 0; i < rs.length(); i++) {
                            JSONObject post1 = rs.optJSONObject(i);
                            String strnamaservice = post1.getString(TAG_NAMASERVICE);
                            String strhargaservice = post1.getString(TAG_HARGASERVICE);
                            String strestimasiservice = post1.getString(TAG_ESTIMASISERVICE);
                            item1 = new GridItemPengiriman();
                            item1.setNamaservice(strnamaservice);
                            item1.setHargaservice(strhargaservice);
                            item1.setEstimasiservice(strestimasiservice);
                            if(getSharedPreferences("DATA",MODE_PRIVATE).getString("namaexpedisi","").equals("JNE")){
                                item1.setGambar("JNE");
                            }
                            else if (getSharedPreferences("DATA",MODE_PRIVATE).getString("namaexpedisi","").equals("TIKI")){
                                item1.setGambar("TIKI");
                            }
                            else{
                                item1.setGambar("POS");
                            }

                            mGridDataProduk.add(item1);
                        }

                        mGridAdapterProduk.setGridData(mGridDataProduk);
                        myJSON=String.valueOf(rs);
                        pd.hide();
                        mGridView1.setVisibility(View.VISIBLE);
                    }

                }catch(JSONException e)
                {
                    e.printStackTrace();
                }
            }
            else {

                Toast.makeText(getApplicationContext(), "Server Maintenance", Toast.LENGTH_SHORT).show();
                Log.e("status",a+"");
            }

        }

    }
}

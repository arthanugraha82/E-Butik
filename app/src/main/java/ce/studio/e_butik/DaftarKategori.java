package ce.studio.e_butik;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class DaftarKategori extends Fragment {

    private static final String TAG_RESULT = "result";
    private static final String TAG_IDKATEGORI = "idkategori";
    private static final String TAG_NAMAKATEGORI = "namakategori";
    private static final String TAG_GAMBAR = "gambar";

    public static String FEED_URL2;

    ProgressDialog pd;

    private GridView mGridView2;
    private GridViewAdapterDaftarKategori mGridAdapterKategori;
    private ArrayList<GridItemDaftarKategori> mGridDataKategori;

    private static final String TAG = DaftarKategori.class.getSimpleName();

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.fragment_daftar_kategori, container, false);

        mGridView2 = (GridView) rootview.findViewById(R.id.gridView2);

        FEED_URL2 = Config.KATEGORI_URL;

        mGridDataKategori = new ArrayList<>();
        mGridAdapterKategori = new GridViewAdapterDaftarKategori(getActivity(), R.layout.grid_item_daftar_kategori, mGridDataKategori);
        mGridView2.setAdapter(mGridAdapterKategori);

        pd = new ProgressDialog(getActivity());
        pd.setMessage("Sedang mengambil data, tunggu sebentar...");
        pd.setCancelable(false);
        pd.show();

        new getKategori().execute(FEED_URL2);

        mGridView2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {

                //Get item at position
                GridItemDaftarKategori item = (GridItemDaftarKategori) parent.getItemAtPosition(position);

                SharedPreferences.Editor editor = getActivity().getSharedPreferences("DATA", Context.MODE_PRIVATE).edit();
                editor.putString("idkategori", item.getIdkategori());
                editor.commit();

                //Pass the image title and url to DetailsActivity
                Intent intent = new Intent(getActivity(), DetailKategori.class);
                startActivity(intent);
            }
        });

        return rootview;
    }


    //Downloading data asynchronously
    public class getKategori extends AsyncTask<String, Void, Integer> {

        @Override
        protected Integer doInBackground(String... params) {
            Integer result2 = 0;
            try {
                // Create Apache HttpClient
                HttpClient httpclient2 = new DefaultHttpClient();
                HttpResponse httpResponse2 = httpclient2.execute(new HttpGet(params[0]));
                int statusCode2 = httpResponse2.getStatusLine().getStatusCode();

                // 200 represents HTTP OK
                if (statusCode2 == 200) {
                    String response2 = streamToString2(httpResponse2.getEntity().getContent());
                    parseResult2(response2);
                    result2 = 1; // Successful
                } else {
                    result2 = 0; //"Failed
                }
            } catch (Exception e) {
                Log.d(TAG, e.getLocalizedMessage());
            }
            return result2;
        }

        @Override
        protected void onPostExecute(Integer result2) {
            // Download complete. Let us update UI
            if (result2 == 1) {
                mGridAdapterKategori.setGridData(mGridDataKategori);

                pd.hide();
            } else {
//                Toast.makeText(getActivity(), "Failed to fetch data!", Toast.LENGTH_SHORT).show();
                pd.hide();
            }


        }
    }

    String streamToString2(InputStream stream2) throws IOException {
        BufferedReader bufferedReader2 = new BufferedReader(new InputStreamReader(stream2));
        String line2;
        String result2 = "";
        while ((line2 = bufferedReader2.readLine()) != null) {
            result2 += line2;
        }

        // Close stream
        if (null != stream2) {
            stream2.close();
        }
        return result2;
    }

    /**
     * Parsing the feed results and get the list
     *
     * @param result2
     */
    private void parseResult2(String result2) {
        try {
            JSONObject response2 = new JSONObject(result2);
            JSONArray posts2 = response2.optJSONArray(TAG_RESULT);
            GridItemDaftarKategori item2;

            for (int i = 0; i < posts2.length(); i++) {
                JSONObject post2 = posts2.optJSONObject(i);
                String stridkategori = post2.getString(TAG_IDKATEGORI);
                String strnamakategori = post2.getString(TAG_NAMAKATEGORI);
                item2 = new GridItemDaftarKategori();
                item2.setIdkategori(stridkategori);
                item2.setNamakategori(strnamakategori);
                if (null != posts2 && posts2.length() > 0) {
                    JSONObject attachment = posts2.getJSONObject(i);
                    if (attachment != null)
                        item2.setGambar(attachment.getString(TAG_GAMBAR));
                }

                mGridDataKategori.add(item2);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
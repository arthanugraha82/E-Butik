package ce.studio.e_butik;

/**
 * Created by Artha on 2/2/2017.
 */
public class GridItemDaftarKategori {

    String idkategori,namakategori,gambar;

    public String getGambar() {

        return gambar;

    }

    public void setGambar(String gambar) {

        this.gambar = gambar;

    }

    public String getIdkategori() {

        return idkategori;

    }

    public void setIdkategori(String idkategori) {

        this.idkategori = idkategori;

    }

    public String getNamakategori() {

        return namakategori;

    }

    public void setNamakategori(String namakategori) {

        this.namakategori = namakategori;

    }
}
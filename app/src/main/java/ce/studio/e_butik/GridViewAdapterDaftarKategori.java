package ce.studio.e_butik;

import android.app.Activity;
import android.content.Context;
import android.text.Html;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class GridViewAdapterDaftarKategori extends ArrayAdapter<GridItemDaftarKategori> {

    private Context mContext;
    private int layoutResourceId;
    private ArrayList<GridItemDaftarKategori> mGridData = new ArrayList<GridItemDaftarKategori>();

    public GridViewAdapterDaftarKategori(Context mContext, int layoutResourceId, ArrayList<GridItemDaftarKategori> mGridData) {
        super(mContext, layoutResourceId, mGridData);
        this.layoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.mGridData = mGridData;
    }


    /**
     * Updates grid data and refresh grid items.
     * @param mGridData
     */
    public void setGridData(ArrayList<GridItemDaftarKategori> mGridData) {
        this.mGridData = mGridData;
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder holder;

        if (row == null) {
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            holder = new ViewHolder();
            holder.idkategori = (TextView) row.findViewById(R.id.idkategori);
            holder.namakategori = (TextView) row.findViewById(R.id.namakategori);
            holder.gambar = (ImageView) row.findViewById(R.id.gambar);
            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();
        }

        GridItemDaftarKategori item = mGridData.get(position);
        holder.idkategori.setText(Html.fromHtml(item.getIdkategori()));
        holder.namakategori.setText(Html.fromHtml(item.getNamakategori()));
        Glide.with(mContext).load(item.getGambar())
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.gambar);

        return row;
    }

    static class ViewHolder {
        TextView idkategori,namakategori;
        ImageView gambar;
    }
}
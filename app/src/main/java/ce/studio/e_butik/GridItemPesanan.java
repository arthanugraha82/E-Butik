package ce.studio.e_butik;

public class GridItemPesanan {

    String idpesanan,kodepesanan,namaproduk,statuspesanan,validpembayaran,gambar,jumlahpesan,hargapesan,totalongkir;

    public String getIdpesanan() {
        return idpesanan;
    }

    public String getJumlahpesan() {
        return jumlahpesan;
    }

    public void setJumlahpesan(String jumlahpesan) {
        this.jumlahpesan = jumlahpesan;
    }

    public String getHargapesan() {
        return hargapesan;
    }

    public void setHargapesan(String hargapesan) {
        this.hargapesan = hargapesan;
    }

    public String getTotalongkir() {
        return totalongkir;
    }

    public void setTotalongkir(String totalongkir) {
        this.totalongkir = totalongkir;
    }

    public void setIdpesanan(String idpesanan) {
        this.idpesanan = idpesanan;
    }

    public String getNamaproduk() {
        return namaproduk;
    }

    public void setNamaproduk(String namaproduk) {
        this.namaproduk = namaproduk;
    }

    public String getKodepesanan() {
        return kodepesanan;
    }

    public String getValidpembayaran() {
        return validpembayaran;
    }

    public void setValidpembayaran(String validpembayaran) {
        this.validpembayaran = validpembayaran;
    }

    public void setKodepesanan(String kodepesanan) {
        this.kodepesanan = kodepesanan;
    }

    public String getStatuspesanan() {
        return statuspesanan;
    }

    public void setStatuspesanan(String statuspesanan) {
        this.statuspesanan = statuspesanan;
    }

    public String getGambar() {
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }
}
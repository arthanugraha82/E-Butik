package ce.studio.e_butik;

import android.app.Activity;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.squareup.picasso.Picasso;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

public class GridViewAdapterDaftarProduk extends ArrayAdapter<GridItemDaftarProduk> {

    private Context mContext;
    private int layoutResourceId;
    private ArrayList<GridItemDaftarProduk> mGridData = new ArrayList<GridItemDaftarProduk>();

    public GridViewAdapterDaftarProduk(Context mContext, int layoutResourceId, ArrayList<GridItemDaftarProduk> mGridData) {
        super(mContext, layoutResourceId, mGridData);
        this.layoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.mGridData = mGridData;
    }


    /**
     * Updates grid data and refresh grid items.
     * @param mGridData
     */
    public void setGridData(ArrayList<GridItemDaftarProduk> mGridData) {
        this.mGridData = mGridData;
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder holder;

        if (row == null) {
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            holder = new ViewHolder();
            holder.idproduk = (TextView) row.findViewById(R.id.idproduk);
            holder.namaproduk = (TextView) row.findViewById(R.id.namaproduk);
            holder.hargaproduk = (TextView) row.findViewById(R.id.hargaproduk);
            holder.bahanproduk = (TextView) row.findViewById(R.id.bahanproduk);
            holder.gambar = (ImageView) row.findViewById(R.id.gambar);
            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();
        }

        GridItemDaftarProduk item = mGridData.get(position);
        holder.idproduk.setText(Html.fromHtml(item.getIdproduk()));
        holder.namaproduk.setText(Html.fromHtml(item.getNama()));
        holder.hargaproduk.setText("Rp. "+String.valueOf(NumberFormat.getNumberInstance(Locale.US).format(Integer.parseInt((item.getHarga())))).replace(",","."));
        holder.bahanproduk.setText("Bahan : "+Html.fromHtml(item.getBahan()));
        Glide.with(mContext).load(item.getGambar())
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.gambar);

        return row;
    }

    static class ViewHolder {
        TextView idproduk,namaproduk,hargaproduk,bahanproduk;
        ImageView gambar;
    }
}
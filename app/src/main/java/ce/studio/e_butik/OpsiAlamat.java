package ce.studio.e_butik;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class OpsiAlamat extends AppCompatActivity {

    public static Activity fa;

    Spinner dataprovinsi,datakota;

    EditText alamatpengiriman;

    Button btnopsipengiriman;

    private static final String TAG_RESULT = "result";
    private static final String TAG_IDPROVINSI= "idprovinsi";
    private static final String TAG_NAMAPROVINSI = "namaprovinsi";
    private static final String TAG_IDKOTA= "idkota";
    private static final String TAG_NAMAKOTA = "namakota";
    static boolean a=false;

    JSONArray rs = null;

    public static String FEED_URL;

    public static String id,provinsi,kota;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_opsi_alamat);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        fa = this;

        dataprovinsi = (Spinner) findViewById(R.id.dataprovinsi);
        datakota = (Spinner) findViewById(R.id.datakota);
        alamatpengiriman = (EditText) findViewById(R.id.alamatpengiriman);
        btnopsipengiriman = (Button) findViewById(R.id.btnopsipengiriman);

        new getdataProvinsi().execute();

        dataprovinsi.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (dataprovinsi.getSelectedItemPosition()==0){
                    datakota.setVisibility(View.GONE);
                }
                else{
                    datakota.setVisibility(View.VISIBLE);
                    TextView tvidprovinsi = (TextView) view.findViewById(R.id.iditem);
                    TextView tvnamaprovinsi = (TextView) view.findViewById(R.id.namaitem);
                    SharedPreferences.Editor editor = getSharedPreferences("DATA", Context.MODE_PRIVATE).edit();
                    editor.putString("idprovinsi", tvidprovinsi.getText().toString());
                    editor.putString("namaprovinsi", tvnamaprovinsi.getText().toString());
                    editor.commit();
                    new getdataKota().execute();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        datakota.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (datakota.getSelectedItemPosition()==0){
                }
                else{
                    TextView tvidkota = (TextView) view.findViewById(R.id.iditem);
                    TextView tvnamakota = (TextView) view.findViewById(R.id.namaitem);
                    SharedPreferences.Editor editor = getSharedPreferences("DATA", Context.MODE_PRIVATE).edit();
                    editor.putString("idkota", tvidkota.getText().toString());
                    editor.putString("namakota", tvnamakota.getText().toString());
                    editor.commit();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btnopsipengiriman.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = getSharedPreferences("DATA", Context.MODE_PRIVATE).edit();
                editor.putString("alamatpengiriman", alamatpengiriman.getText().toString());
                editor.commit();
                startActivity(new Intent(OpsiAlamat.this,OpsiPengiriman.class));
            }
        });
    }

    public boolean onOptionsItemSelected(MenuItem item){
        if(item.getItemId()==android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }

    private class getdataProvinsi extends AsyncTask<String, String, JSONObject> {
        @Override
        protected void onPreExecute(){

        }
        @Override
        protected JSONObject doInBackground(String... args)
        {
            FEED_URL = Config.PROVINSI_URL;

            Log.e("URL Provinsi =",FEED_URL);

            JSONParser jParser = new JSONParser();

            JSONObject json = jParser.getJSONFromUrl(FEED_URL);
            if(json==null)
            {
                a=false;
            }
            else a=true;
            return json;
        }
        protected void onPostExecute(JSONObject json) {
            if(a==true)
            {
                try{
                    Log.e("status",a+"");
                    rs = json.getJSONArray(TAG_RESULT);

                    String[] idprovinsi = new String[rs.length()];
                    String[] namaprovinsi = new String[rs.length()];

                    idprovinsi[0] = "0";
                    namaprovinsi[0] = "Pilih Provinsi";

                    for(int i=1; i<rs.length();i++)
                    {
                        JSONObject a = rs.getJSONObject(i);
                        String stridprovinsi = a.getString(TAG_IDPROVINSI);
                        String strnamaprovinsi = a.getString(TAG_NAMAPROVINSI);

                        idprovinsi[i] = stridprovinsi;
                        namaprovinsi[i] = strnamaprovinsi;

                    }
                    SpinnerAdapter customAdapter=new SpinnerAdapter(getApplicationContext(),idprovinsi,namaprovinsi);
                    dataprovinsi.setAdapter(customAdapter);

                }catch(JSONException e)
                {
                    e.printStackTrace();
                }
            }
            else {

                Toast.makeText(getApplicationContext(), "Server Maintenance", Toast.LENGTH_SHORT).show();
                Log.e("status",a+"");
            }

        }

    }

    private class getdataKota extends AsyncTask<String, String, JSONObject> {
        @Override
        protected void onPreExecute(){

        }
        @Override
        protected JSONObject doInBackground(String... args)
        {
            FEED_URL = Config.KOTA_URL;
            FEED_URL += "?idprovinsi="+getSharedPreferences("DATA",MODE_PRIVATE).getString("idprovinsi","");

            Log.e("URL Kota =",FEED_URL);

            JSONParser jParser = new JSONParser();

            JSONObject json = jParser.getJSONFromUrl(FEED_URL);
            if(json==null)
            {
                a=false;
            }
            else a=true;
            return json;
        }
        protected void onPostExecute(JSONObject json) {
            if(a==true)
            {
                try{
                    Log.e("status",a+"");
                    rs = json.getJSONArray(TAG_RESULT);

                    if(rs.length()>0){
                        String[] idkota = new String[rs.length()];
                        String[] namakota = new String[rs.length()];

                        idkota[0] = "0";
                        namakota[0] = "Pilih Kota";

                        for(int i=1; i<rs.length();i++)
                        {
                            JSONObject a = rs.getJSONObject(i);
                            String stridkota = a.getString(TAG_IDKOTA);
                            String strnamakota = a.getString(TAG_NAMAKOTA);

                            idkota[i] = stridkota;
                            namakota[i] = strnamakota;

                        }
                        SpinnerAdapter customAdapter=new SpinnerAdapter(getApplicationContext(),idkota,namakota);
                        datakota.setAdapter(customAdapter);
                    }
                    else{
                        String[] idkota = new String[1];
                        String[] namakota = new String[1];

                        idkota[0] = "0";
                        namakota[0] = "Pilih Kota";

                        SpinnerAdapter customAdapter=new SpinnerAdapter(getApplicationContext(),idkota,namakota);
                        datakota.setAdapter(customAdapter);
                    }


                }catch(JSONException e)
                {
                    e.printStackTrace();
                }
            }
            else {

                Toast.makeText(getApplicationContext(), "Server Maintenance", Toast.LENGTH_SHORT).show();
                Log.e("status",a+"");
            }

        }

    }
}
